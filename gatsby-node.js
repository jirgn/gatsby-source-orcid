const readPublic = require('./src/readPublic');
const {
  normalizePerson,
  normalizeActivities,
  normalizeWork,
  normalizePeerReview,
  normalizeFunding,
} = require('./src/normalize');

const PERSON_NODE_TYPE = 'OrcidPerson';
const ACTIVITIES_NODE_TYPE = 'OrcidActivities';
const WORK_NODE_TYPE = 'OrcidWork';
const PEER_REVIEW_NODE_TYPE = 'OrcidPeerReview';
const FUNDING_NODE_TYPE = 'OrcidFunding';

exports.onPreInit = () => {
  console.info('Loading gatsby-source-orcid');
};

exports.pluginOptionsSchema = ({ Joi }) => {
  return Joi.object({
    isSandbox: Joi.boolean().default(false),
    orcid: Joi.string()
      .required()
      .description(
        'the orcid of the record to import. Example: 0000-0003-0040-8578'
      ),
  });
};

exports.sourceNodes = async (
  { actions, createContentDigest, createNodeId },
  { orcid, isSandbox }
) => {
  const { createNode } = actions;

  const personResponse = await readPublic.person(orcid, isSandbox);
  const person = normalizePerson(personResponse);
  createNode({
    ...person,
    id: createNodeId(`${PERSON_NODE_TYPE}-${person.path}`),
    parent: null,
    children: [],
    internal: {
      type: PERSON_NODE_TYPE,
      content: JSON.stringify(person),
      contentDigest: createContentDigest(person),
    },
  });

  const activitiesResponse = await readPublic.activities(orcid, isSandbox);
  const activities = normalizeActivities(activitiesResponse);
  createNode({
    ...activities,
    id: createNodeId(`${ACTIVITIES_NODE_TYPE}-${activities.path}`),
    parent: null,
    children: [],
    internal: {
      type: ACTIVITIES_NODE_TYPE,
      content: JSON.stringify(activities),
      contentDigest: createContentDigest(activities),
    },
  });

  const workCodes = activities.works.map(item => item.putCode);
  const worksBulkResponse = await readPublic.worksBulk(
    orcid,
    workCodes,
    isSandbox
  );
  for (const workResponse of worksBulkResponse) {
    const work = normalizeWork(workResponse.work);
    createNode({
      ...work,
      id: createNodeId(`${WORK_NODE_TYPE}-${work.path}`),
      parent: null,
      children: [],
      internal: {
        type: WORK_NODE_TYPE,
        content: JSON.stringify(work),
        contentDigest: createContentDigest(work),
      },
    });
  }

  const peerReviewCodes = activities.peerReviews.map(item => item.putCode);
  const peerReviews = await readPublic.peerReviews(
    orcid,
    peerReviewCodes,
    isSandbox
  );
  for (const peerReviewResponse of peerReviews) {
    const peerReview = normalizePeerReview(peerReviewResponse);
    createNode({
      ...peerReview,
      id: createNodeId(`${PEER_REVIEW_NODE_TYPE}-${peerReview.path}`),
      parent: null,
      children: [],
      internal: {
        type: PEER_REVIEW_NODE_TYPE,
        content: JSON.stringify(peerReview),
        contentDigest: createContentDigest(peerReview),
      },
    });
  }

  const fundingCodes = activities.fundings.map(item => item.putCode);
  const fundings = await readPublic.fundings(orcid, fundingCodes, isSandbox);
  for (const fundingResponse of fundings) {
    const funding = normalizeFunding(fundingResponse);
    createNode({
      ...funding,
      id: createNodeId(`${FUNDING_NODE_TYPE}-${funding.path}`),
      parent: null,
      children: [],
      internal: {
        type: FUNDING_NODE_TYPE,
        content: JSON.stringify(funding),
        contentDigest: createContentDigest(funding),
      },
    });
  }
};

exports.createSchemaCustomization = ({ actions }) => {
  const { createTypes } = actions;

  createTypes(`
    type OrcidIdentifier {
      type: String!,
      value: String!,
      url: String
    }

    type OrcidDate {
      year: Int
      month: Int
      day: Int
    }

    type OrcidAmount {
      value: Int!
      currencyCode: String!
    }

    type OrcidContributor {
      creditName: String,
      role: String
    }

    type OrcidWorkTitle {
      main: String!
      subtitle: String
    }

    type OrcidOrganization {
      name: String!
      address: OrcidAddress
    }

    type OrcidAddress {
      city: String
      region: String
      country: String
    }

    type ${PERSON_NODE_TYPE} implements Node {
      id: String!
      orcid: String!
      path: String!
      putCode: Int!
      givenNames: String!
      familyName: String!
      biography: String
      identifiers: [OrcidIdentifier]!
      activities: OrcidActivities @link(by: "path")
    }

    type ${FUNDING_NODE_TYPE} implements Node {
      id: String!
      orcid: String!
      path: String!
      putCode: Int!
      type: String!
      shortDescription: String
      amount: OrcidAmount
      url: String
      startDate: OrcidDate
      endDate: OrcidDate
      organization: OrcidOrganization
    }

    type ${PEER_REVIEW_NODE_TYPE} implements Node {
      id: String!
      orcid: String!
      path: String!
      putCode: Int!
      reviewerRole: String
      reviewUrl: String
      reviewType: String
      reviewCompletionDate: OrcidDate
      subjectContainerName: String
      subjectType: String
      subjectName: String
      subjectUrl: String
    }

    type ${WORK_NODE_TYPE} implements Node {
      id: String!
      orcid: String!
      path: String!
      putCode: Int!
      title: OrcidWorkTitle!
      journalTitle: String
      shortDescription: String
      type: String!
      publicationDate: OrcidDate
      externalIds: [ OrcidIdentifier ]
      url: String
      contributors: [OrcidContributor]!
    }

    type ${ACTIVITIES_NODE_TYPE} implements Node {
      id: String!
      orcid: String!
      path: String!
      putCode: Int!
      works: [${WORK_NODE_TYPE}]! @link(by:"path", from: "works.path")
      fundings: [${FUNDING_NODE_TYPE}]! @link(by:"path", from: "fundings.path")
      peerReviews: [${PEER_REVIEW_NODE_TYPE}]! @link(by:"path", from: "peerReviews.path")
    }
  `);
};
