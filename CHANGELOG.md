# [1.1.0](https://gitlab.com/jirgn/gatsby-source-orcid/compare/v1.0.2...v1.1.0) (2021-04-28)


### Bug Fixes

* minor wording issues ([2c35652](https://gitlab.com/jirgn/gatsby-source-orcid/commit/2c3565256ae7476d4b979456241428d8e79d8a9c))


### Features

* add externalIds to Work ([b0fd524](https://gitlab.com/jirgn/gatsby-source-orcid/commit/b0fd5247f65a170e243d63944e86c5a1ea258fbe))

## [1.0.2](https://gitlab.com/jirgn/gatsby-source-orcid/compare/v1.0.1...v1.0.2) (2021-03-27)


### Bug Fixes

* options validation ([65d0271](https://gitlab.com/jirgn/gatsby-source-orcid/commit/65d0271f10f86ed17407cfb27cd79bb05cc7d656))

## [1.0.1](https://gitlab.com/jirgn/gatsby-source-orcid/compare/v1.0.0...v1.0.1) (2021-01-07)

# 1.0.0 (2021-01-05)


### Bug Fixes

* activate semantic-release/npm to trigger publish ([652c34a](https://gitlab.com/jirgn/gatsby-source-orcid/commit/652c34a9254ba49cb9130d616d33755ce5c84a7f))
* remove CHANGELOG and reset version ([7076569](https://gitlab.com/jirgn/gatsby-source-orcid/commit/707656915e8c8429aa5634daf0fd927e7cf700e9))
* **project level release:** try release ([d8b0dea](https://gitlab.com/jirgn/gatsby-source-orcid/commit/d8b0dea0ef4014bb1d1f29f586d7326daabb09f2))
* **project scope:** npm publish only works in project scope ([f345277](https://gitlab.com/jirgn/gatsby-source-orcid/commit/f345277b887cfa943efe474f52fcd0b2fa02f335))
* syntax issue in .gitlab-ci.yml ([36f7f5d](https://gitlab.com/jirgn/gatsby-source-orcid/commit/36f7f5ddba6c21dafd94909593cab24fe971d190))
