YARN=`which yarn`

.PHONY: help
help:             ## Show this help
	@fgrep -h "##" $(MAKEFILE_LIST) | fgrep -v fgrep | sed -e 's/\\$$//' | sed -e 's/##//'

.PHONY: install
install:          ## install dependencies
	${YARN}

.PHONY: semantic-release
semantic-release: ## publish semantic release (detect version, tag version, build changelog)
	${YARN} semantic-release

.PHONY: test
test:             ## run all tests
	${YARN} test

.PHONY: format
format:           ## reformat src code to fit standard formatting
	${YARN} format

.PHONY: check-style
check-style:      ## check codestyle
	${YARN} lint
	./node_modules/.bin/eslint

