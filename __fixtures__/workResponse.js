const workAll = {
  'created-date': {
    value: 1592839089729,
  },
  'last-modified-date': {
    value: 1605860629506,
  },
  source: {
    'source-orcid': null,
    'source-client-id': {
      uri: 'https://orcid.org/client/0000-0001-9884-1913',
      path: '0000-0001-9884-1913',
      host: 'orcid.org',
    },
    'source-name': {
      value: 'Crossref',
    },
    'assertion-origin-orcid': null,
    'assertion-origin-client-id': null,
    'assertion-origin-name': null,
  },
  'put-code': 76054202,
  path: '/0000-0002-1025-9484/work/76054202',
  title: {
    title: {
      value:
        'Four Phosphates at One Blow: Access to Pentaphosphorylated Magic Spot Nucleotides and Their Analysis by Capillary Electrophoresis',
    },
    subtitle: null,
    'translated-title': null,
  },
  'journal-title': {
    value: 'The Journal of Organic Chemistry',
  },
  'short-description': null,
  citation: {
    'citation-type': 'bibtex',
    'citation-value':
      '@article{Haas_2020,\n\tdoi = {10.1021/acs.joc.0c00841},\n\turl = {https://doi.org/10.1021%2Facs.joc.0c00841},\n\tyear = 2020,\n\tmonth = {jun},\n\tpublisher = {American Chemical Society ({ACS})},\n\tauthor = {Thomas M. Haas and Danye Qiu and Markus HÃ¤ner and Larissa Angebauer and Alexander Ripp and Jyoti Singh and Hans-Georg Koch and Claudia Jessen-Trefzer and Henning J. Jessen},\n\ttitle = {Four Phosphates at One Blow: Access to Pentaphosphorylated Magic Spot Nucleotides and Their Analysis by Capillary Electrophoresis},\n\tjournal = {The Journal of Organic Chemistry}\n}',
  },
  type: 'journal-article',
  'publication-date': {
    year: {
      value: '2020',
    },
    month: {
      value: '11',
    },
    day: {
      value: '20',
    },
  },
  'external-ids': {
    'external-id': [
      {
        'external-id-type': 'doi',
        'external-id-value': '10.1021/acs.joc.0c00841',
        'external-id-normalized': {
          value: '10.1021/acs.joc.0c00841',
          transient: true,
        },
        'external-id-normalized-error': null,
        'external-id-url': {
          value: 'https://doi.org/10.1021/acs.joc.0c00841',
        },
        'external-id-relationship': 'self',
      },
    ],
  },
  url: {
    value: 'https://doi.org/10.1021/acs.joc.0c00841',
  },
  contributors: {
    contributor: [
      {
        'contributor-orcid': null,
        'credit-name': {
          value: 'Thomas M. Haas',
        },
        'contributor-email': null,
        'contributor-attributes': {
          'contributor-sequence': null,
          'contributor-role': 'author',
        },
      },
      {
        'contributor-orcid': null,
        'credit-name': {
          value: 'Danye Qiu',
        },
        'contributor-email': null,
        'contributor-attributes': {
          'contributor-sequence': null,
          'contributor-role': 'author',
        },
      },
      {
        'contributor-orcid': null,
        'credit-name': {
          value: 'Markus Häner',
        },
        'contributor-email': null,
        'contributor-attributes': {
          'contributor-sequence': null,
          'contributor-role': 'author',
        },
      },
      {
        'contributor-orcid': null,
        'credit-name': {
          value: 'Larissa Angebauer',
        },
        'contributor-email': null,
        'contributor-attributes': {
          'contributor-sequence': null,
          'contributor-role': 'author',
        },
      },
      {
        'contributor-orcid': null,
        'credit-name': {
          value: 'Alexander Ripp',
        },
        'contributor-email': null,
        'contributor-attributes': {
          'contributor-sequence': null,
          'contributor-role': 'author',
        },
      },
      {
        'contributor-orcid': null,
        'credit-name': {
          value: 'Jyoti Singh',
        },
        'contributor-email': null,
        'contributor-attributes': {
          'contributor-sequence': null,
          'contributor-role': 'author',
        },
      },
      {
        'contributor-orcid': null,
        'credit-name': {
          value: 'Hans-Georg Koch',
        },
        'contributor-email': null,
        'contributor-attributes': {
          'contributor-sequence': null,
          'contributor-role': 'author',
        },
      },
      {
        'contributor-orcid': null,
        'credit-name': {
          value: 'Claudia Jessen-Trefzer',
        },
        'contributor-email': null,
        'contributor-attributes': {
          'contributor-sequence': null,
          'contributor-role': 'author',
        },
      },
      {
        'contributor-orcid': null,
        'credit-name': {
          value: 'Henning J. Jessen',
        },
        'contributor-email': null,
        'contributor-attributes': {
          'contributor-sequence': null,
          'contributor-role': 'author',
        },
      },
    ],
  },
  'language-code': null,
  country: null,
  visibility: 'public',
};

module.exports = {
  workAll
};
