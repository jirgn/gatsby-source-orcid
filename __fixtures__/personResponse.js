const personAll = {
  'last-modified-date': {
    value: 1609170697924,
  },
  name: {
    'created-date': {
      value: 1608670138202,
    },
    'last-modified-date': {
      value: 1608670138202,
    },
    'given-names': {
      value: 'Jirgn',
    },
    'family-name': {
      value: 'Mssnr',
    },
    'credit-name': null,
    source: null,
    visibility: 'public',
    path: '0000-0003-0040-8578',
  },
  'external-identifiers': {
    'external-identifier': [
      {
        'created-date': {
          value: 1399279100312,
        },
        'display-index': 0,
        'external-id-relationship': 'self',
        'external-id-type': 'ResearcherID',
        'external-id-url': {
          value: 'http://www.researcherid.com/rid/F-5108-2014',
        },
        'external-id-value': 'F-5108-2014',
        'last-modified-date': {
          value: 1517869567419,
        },
        path: '/0000-0002-1025-9484/external-identifiers/89612',
        'put-code': 89612,
        source: {
          'assertion-origin-client-id': null,
          'assertion-origin-name': {
            value: 'Henning Jessen',
          },
          'assertion-origin-orcid': {
            host: 'orcid.org',
            path: '0000-0002-1025-9484',
            uri: 'https://orcid.org/0000-0002-1025-9484',
          },
          'source-client-id': {
            host: 'orcid.org',
            path: '0000-0003-1377-5676',
            uri: 'https://orcid.org/client/0000-0003-1377-5676',
          },
          'source-name': {
            value: 'ResearcherID',
          },
          'source-orcid': null,
        },
        visibility: 'public',
      },
      {
        'created-date': {
          value: 1399281320465,
        },
        'display-index': 0,
        'external-id-relationship': 'self',
        'external-id-type': 'Scopus Author ID',
        'external-id-url': {
          value:
            'http://www.scopus.com/inward/authorDetails.url?authorID=10641609900&partnerID=MN8TOARS',
        },
        'external-id-value': '10641609900',
        'last-modified-date': {
          value: 1517873256636,
        },
        path: '/0000-0002-1025-9484/external-identifiers/197577',
        'put-code': 197577,
        source: {
          'assertion-origin-client-id': null,
          'assertion-origin-name': {
            value: 'Henning Jessen',
          },
          'assertion-origin-orcid': {
            host: 'orcid.org',
            path: '0000-0002-1025-9484',
            uri: 'https://orcid.org/0000-0002-1025-9484',
          },
          'source-client-id': {
            host: 'orcid.org',
            path: '0000-0002-5982-8983',
            uri: 'https://orcid.org/client/0000-0002-5982-8983',
          },
          'source-name': {
            value: 'Scopus - Elsevier',
          },
          'source-orcid': null,
        },
        visibility: 'public',
      },
    ],
    'last-modified-date': {
      value: 1517873256636,
    },
    path: '/0000-0002-1025-9484/external-identifiers',
  },

  'other-names': {
    'last-modified-date': {
      value: 1609164088200,
    },
    'other-name': [
      {
        'created-date': {
          value: 1609164088200,
        },
        'last-modified-date': {
          value: 1609164088200,
        },
        source: {
          'source-orcid': {
            uri: 'https://sandbox.orcid.org/0000-0003-0040-8578',
            path: '0000-0003-0040-8578',
            host: 'sandbox.orcid.org',
          },
          'source-client-id': null,
          'source-name': {
            value: 'Jirgn Mssnr',
          },
          'assertion-origin-orcid': null,
          'assertion-origin-client-id': null,
          'assertion-origin-name': null,
        },
        content: 'advanced chemistry',
        visibility: 'public',
        path: '/0000-0003-0040-8578/other-names/17205',
        'put-code': 17205,
        'display-index': 2,
      },
    ],
    path: '/0000-0003-0040-8578/other-names',
  },
  biography: {
    'created-date': {
      value: 1609172826903,
    },
    'last-modified-date': {
      value: 1609172839166,
    },
    content:
      'This is the Biography.\nHere is some simple text that describes my live.',
    visibility: 'public',
    path: '/0000-0003-0040-8578/biography',
  },
  'researcher-urls': {
    'last-modified-date': {
      value: 1609164257352,
    },
    'researcher-url': [
      {
        'created-date': {
          value: 1609164257352,
        },
        'last-modified-date': {
          value: 1609164257352,
        },
        source: {
          'source-orcid': {
            uri: 'https://sandbox.orcid.org/0000-0003-0040-8578',
            path: '0000-0003-0040-8578',
            host: 'sandbox.orcid.org',
          },
          'source-client-id': null,
          'source-name': {
            value: 'Jirgn Mssnr',
          },
          'assertion-origin-orcid': null,
          'assertion-origin-client-id': null,
          'assertion-origin-name': null,
        },
        'url-name': 'research-lab',
        url: {
          value: 'https://trefzer-group.research-lab.org/',
        },
        visibility: 'public',
        path: '/0000-0003-0040-8578/researcher-urls/44556',
        'put-code': 44556,
        'display-index': 3,
      },
    ],
    path: '/0000-0003-0040-8578/researcher-urls',
  },
  emails: {
    'last-modified-date': {
      value: 1609170697924,
    },
    email: [
      {
        'created-date': {
          value: 1608670418014,
        },
        'last-modified-date': {
          value: 1609170697924,
        },
        source: {
          'source-orcid': {
            uri: 'https://sandbox.orcid.org/0000-0003-0040-8578',
            path: '0000-0003-0040-8578',
            host: 'sandbox.orcid.org',
          },
          'source-client-id': null,
          'source-name': {
            value: 'Jirgn Mssnr',
          },
          'assertion-origin-orcid': null,
          'assertion-origin-client-id': null,
          'assertion-origin-name': null,
        },
        email: 'jirgn@mailinator.com',
        path: null,
        visibility: 'public',
        verified: true,
        primary: true,
        'put-code': null,
      },
    ],
    path: '/0000-0003-0040-8578/email',
  },
  addresses: {
    'last-modified-date': null,
    address: [],
    path: '/0000-0003-0040-8578/address',
  },
  keywords: {
    'last-modified-date': null,
    keyword: [],
    path: '/0000-0003-0040-8578/keywords',
  },
  path: '/0000-0003-0040-8578/person',
};

module.exports = {
  personAll,
};
