const activitiesWithWorks = {
  path: '/0000-0003-0040-8578/activities',
  works: {
    'last-modified-date': {
      value: 1608765156874,
    },
    group: [
      {
        'last-modified-date': {
          value: 1608678201108,
        },
        'external-ids': {
          'external-id': [
            {
              'external-id-type': 'pmid',
              'external-id-value': '33013801',
              'external-id-normalized': {
                value: '33013801',
                transient: true,
              },
              'external-id-normalized-error': null,
              'external-id-url': {
                value: 'https://www.ncbi.nlm.nih.gov/pubmed/33013801',
              },
              'external-id-relationship': 'self',
            },
            {
              'external-id-type': 'pmc',
              'external-id-value': 'PMC7516205',
              'external-id-normalized': {
                value: 'PMC7516205',
                transient: true,
              },
              'external-id-normalized-error': null,
              'external-id-url': {
                value: 'https://europepmc.org/articles/PMC7516205',
              },
              'external-id-relationship': 'self',
            },
            {
              'external-id-type': 'doi',
              'external-id-value': '10.3389/fmicb.2020.570606',
              'external-id-normalized': {
                value: '10.3389/fmicb.2020.570606',
                transient: true,
              },
              'external-id-normalized-error': null,
              'external-id-url': {
                value: 'https://doi.org/10.3389/fmicb.2020.570606',
              },
              'external-id-relationship': 'self',
            },
          ],
        },
        'work-summary': [
          {
            'put-code': 1177076,
            'created-date': {
              value: 1608672765365,
            },
            'last-modified-date': {
              value: 1608678201108,
            },
            source: {
              'source-orcid': {
                uri: 'https://sandbox.orcid.org/0000-0003-0040-8578',
                path: '0000-0003-0040-8578',
                host: 'sandbox.orcid.org',
              },
              'source-client-id': null,
              'source-name': {
                value: 'Jirgn Mssnr',
              },
              'assertion-origin-orcid': null,
              'assertion-origin-client-id': null,
              'assertion-origin-name': null,
            },
            title: {
              title: {
                value:
                  'The Two-Component Locus MSMEG_0244/0246 Together With MSMEG_0243 Affects Biofilm Assembly in M. smegmatis Correlating With Changes in Phosphatidylinositol Mannosides Acylation.',
              },
              subtitle: null,
              'translated-title': {
                value: 'der deutsche titel',
                'language-code': 'de',
              },
            },
            'external-ids': {
              'external-id': [
                {
                  'external-id-type': 'pmid',
                  'external-id-value': '33013801',
                  'external-id-normalized': {
                    value: '33013801',
                    transient: true,
                  },
                  'external-id-normalized-error': null,
                  'external-id-url': {
                    value: 'https://www.ncbi.nlm.nih.gov/pubmed/33013801',
                  },
                  'external-id-relationship': 'self',
                },
                {
                  'external-id-type': 'pmc',
                  'external-id-value': 'PMC7516205',
                  'external-id-normalized': {
                    value: 'PMC7516205',
                    transient: true,
                  },
                  'external-id-normalized-error': null,
                  'external-id-url': {
                    value: 'https://europepmc.org/articles/PMC7516205',
                  },
                  'external-id-relationship': 'self',
                },
                {
                  'external-id-type': 'doi',
                  'external-id-value': '10.3389/fmicb.2020.570606',
                  'external-id-normalized': {
                    value: '10.3389/fmicb.2020.570606',
                    transient: true,
                  },
                  'external-id-normalized-error': null,
                  'external-id-url': {
                    value: 'https://doi.org/10.3389/fmicb.2020.570606',
                  },
                  'external-id-relationship': 'self',
                },
              ],
            },
            url: {
              value: 'https://europepmc.org/articles/PMC7516205',
            },
            type: 'journal-article',
            'publication-date': {
              year: {
                value: '2020',
              },
              month: {
                value: '09',
              },
              day: {
                value: '11',
              },
            },
            'journal-title': {
              value: 'Frontiers in microbiology',
            },
            visibility: 'public',
            path: '/0000-0003-0040-8578/work/1177076',
            'display-index': '1',
          },
        ],
      },
      {
        'last-modified-date': {
          value: 1608674956872,
        },
        'external-ids': {
          'external-id': [
            {
              'external-id-type': 'doi',
              'external-id-value': '10.1021/acscentsci.8b00962',
              'external-id-normalized': {
                value: '10.1021/acscentsci.8b00962',
                transient: true,
              },
              'external-id-normalized-error': null,
              'external-id-url': {
                value: 'https://doi.org/10.1021/acscentsci.8b00962',
              },
              'external-id-relationship': 'self',
            },
            {
              'external-id-type': 'pmc',
              'external-id-value': 'PMC6487467',
              'external-id-normalized': {
                value: 'PMC6487467',
                transient: true,
              },
              'external-id-normalized-error': null,
              'external-id-url': {
                value: 'https://europepmc.org/articles/PMC6487467',
              },
              'external-id-relationship': 'self',
            },
            {
              'external-id-type': 'pmid',
              'external-id-value': '31041384',
              'external-id-normalized': {
                value: '31041384',
                transient: true,
              },
              'external-id-normalized-error': null,
              'external-id-url': {
                value: 'https://www.ncbi.nlm.nih.gov/pubmed/31041384',
              },
              'external-id-relationship': 'self',
            },
          ],
        },
        'work-summary': [
          {
            'put-code': 1177077,
            'created-date': {
              value: 1608672912248,
            },
            'last-modified-date': {
              value: 1608674956872,
            },
            source: {
              'source-orcid': {
                uri: 'https://sandbox.orcid.org/0000-0003-0040-8578',
                path: '0000-0003-0040-8578',
                host: 'sandbox.orcid.org',
              },
              'source-client-id': null,
              'source-name': {
                value: 'Jirgn Mssnr',
              },
              'assertion-origin-orcid': null,
              'assertion-origin-client-id': null,
              'assertion-origin-name': null,
            },
            title: {
              title: {
                value:
                  'Trehalose Conjugation Enhances Toxicity of Photosensitizers against Mycobacteria.',
              },
              subtitle: null,
              'translated-title': null,
            },
            'external-ids': {
              'external-id': [
                {
                  'external-id-type': 'pmid',
                  'external-id-value': '31041384',
                  'external-id-normalized': {
                    value: '31041384',
                    transient: true,
                  },
                  'external-id-normalized-error': null,
                  'external-id-url': {
                    value: 'https://www.ncbi.nlm.nih.gov/pubmed/31041384',
                  },
                  'external-id-relationship': 'self',
                },
                {
                  'external-id-type': 'pmc',
                  'external-id-value': 'PMC6487467',
                  'external-id-normalized': {
                    value: 'PMC6487467',
                    transient: true,
                  },
                  'external-id-normalized-error': null,
                  'external-id-url': {
                    value: 'https://europepmc.org/articles/PMC6487467',
                  },
                  'external-id-relationship': 'self',
                },
                {
                  'external-id-type': 'doi',
                  'external-id-value': '10.1021/acscentsci.8b00962',
                  'external-id-normalized': {
                    value: '10.1021/acscentsci.8b00962',
                    transient: true,
                  },
                  'external-id-normalized-error': null,
                  'external-id-url': {
                    value: 'https://doi.org/10.1021/acscentsci.8b00962',
                  },
                  'external-id-relationship': 'self',
                },
              ],
            },
            url: {
              value: 'https://europepmc.org/articles/PMC6487467',
            },
            type: 'journal-article',
            'publication-date': {
              year: {
                value: '2019',
              },
              month: {
                value: '03',
              },
              day: {
                value: '27',
              },
            },
            'journal-title': {
              value: 'ACS central science',
            },
            visibility: 'public',
            path: '/0000-0003-0040-8578/work/1177077',
            'display-index': '1',
          },
        ],
      },
      {
        'last-modified-date': {
          value: 1608761238063,
        },
        'external-ids': {
          'external-id': [
            {
              'external-id-type': 'pmid',
              'external-id-value': '29148157',
              'external-id-normalized': {
                value: '29148157',
                transient: true,
              },
              'external-id-normalized-error': null,
              'external-id-url': {
                value: 'https://www.ncbi.nlm.nih.gov/pubmed/29148157',
              },
              'external-id-relationship': 'self',
            },
            {
              'external-id-type': 'pmc',
              'external-id-value': 'PMC6324189',
              'external-id-normalized': {
                value: 'PMC6324189',
                transient: true,
              },
              'external-id-normalized-error': null,
              'external-id-url': {
                value: 'https://europepmc.org/articles/PMC6324189',
              },
              'external-id-relationship': 'self',
            },
            {
              'external-id-type': 'doi',
              'external-id-value': '10.1002/cbic.201700428',
              'external-id-normalized': {
                value: '10.1002/cbic.201700428',
                transient: true,
              },
              'external-id-normalized-error': null,
              'external-id-url': {
                value: 'https://doi.org/10.1002/cbic.201700428',
              },
              'external-id-relationship': 'self',
            },
          ],
        },
        'work-summary': [
          {
            'put-code': 1177314,
            'created-date': {
              value: 1608761238063,
            },
            'last-modified-date': {
              value: 1608761238063,
            },
            source: {
              'source-orcid': {
                uri: 'https://sandbox.orcid.org/0000-0003-0040-8578',
                path: '0000-0003-0040-8578',
                host: 'sandbox.orcid.org',
              },
              'source-client-id': null,
              'source-name': {
                value: 'Jirgn Mssnr',
              },
              'assertion-origin-orcid': null,
              'assertion-origin-client-id': null,
              'assertion-origin-name': null,
            },
            title: {
              title: {
                value:
                  'New WS9326A Derivatives and One New Annimycin Derivative with Antimalarial Activity are Produced by Streptomyces asterosporus DSM 41452 and Its Mutant.',
              },
              subtitle: null,
              'translated-title': null,
            },
            'external-ids': {
              'external-id': [
                {
                  'external-id-type': 'pmid',
                  'external-id-value': '29148157',
                  'external-id-normalized': {
                    value: '29148157',
                    transient: true,
                  },
                  'external-id-normalized-error': null,
                  'external-id-url': {
                    value: 'https://www.ncbi.nlm.nih.gov/pubmed/29148157',
                  },
                  'external-id-relationship': 'self',
                },
                {
                  'external-id-type': 'pmc',
                  'external-id-value': 'PMC6324189',
                  'external-id-normalized': {
                    value: 'PMC6324189',
                    transient: true,
                  },
                  'external-id-normalized-error': null,
                  'external-id-url': {
                    value: 'https://europepmc.org/articles/PMC6324189',
                  },
                  'external-id-relationship': 'self',
                },
                {
                  'external-id-type': 'doi',
                  'external-id-value': '10.1002/cbic.201700428',
                  'external-id-normalized': {
                    value: '10.1002/cbic.201700428',
                    transient: true,
                  },
                  'external-id-normalized-error': null,
                  'external-id-url': {
                    value: 'https://doi.org/10.1002/cbic.201700428',
                  },
                  'external-id-relationship': 'self',
                },
              ],
            },
            url: {
              value: 'https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6324189',
            },
            type: 'journal-article',
            'publication-date': {
              year: {
                value: '2017',
              },
              month: {
                value: '12',
              },
              day: {
                value: '18',
              },
            },
            'journal-title': {
              value: 'Chembiochem : a European journal of chemical biology',
            },
            visibility: 'public',
            path: '/0000-0003-0040-8578/work/1177314',
            'display-index': '1',
          },
        ],
      },
      {
        'last-modified-date': {
          value: 1608765156874,
        },
        'external-ids': {
          'external-id': [
            {
              'external-id-type': 'handle',
              'external-id-value': 'Identiifier',
              'external-id-normalized': {
                value: 'identiifier',
                transient: true,
              },
              'external-id-normalized-error': null,
              'external-id-url': {
                value: 'http://someurl/to/the/book',
              },
              'external-id-relationship': 'self',
            },
          ],
        },
        'work-summary': [
          {
            'put-code': 1177316,
            'created-date': {
              value: 1608765105138,
            },
            'last-modified-date': {
              value: 1608765156874,
            },
            source: {
              'source-orcid': {
                uri: 'https://sandbox.orcid.org/0000-0003-0040-8578',
                path: '0000-0003-0040-8578',
                host: 'sandbox.orcid.org',
              },
              'source-client-id': null,
              'source-name': {
                value: 'Jirgn Mssnr',
              },
              'assertion-origin-orcid': null,
              'assertion-origin-client-id': null,
              'assertion-origin-name': null,
            },
            title: {
              title: {
                value: 'The Great Book',
              },
              subtitle: null,
              'translated-title': {
                value: 'Top Titel',
                'language-code': 'de',
              },
            },
            'external-ids': {
              'external-id': [
                {
                  'external-id-type': 'handle',
                  'external-id-value': 'Identiifier',
                  'external-id-normalized': {
                    value: 'identiifier',
                    transient: true,
                  },
                  'external-id-normalized-error': null,
                  'external-id-url': {
                    value: 'http://someurl/to/the/book',
                  },
                  'external-id-relationship': 'self',
                },
              ],
            },
            url: null,
            type: 'book',
            'publication-date': {
              year: {
                value: '2008',
              },
              month: {
                value: '11',
              },
              day: {
                value: '11',
              },
            },
            'journal-title': {
              value: 'Free Press',
            },
            visibility: 'public',
            path: '/0000-0003-0040-8578/work/1177316',
            'display-index': '1',
          },
        ],
      },
    ],
    path: '/0000-0003-0040-8578/works',
  },
};

const activitiesWithPeerReviews = {
  path: '/0000-0003-0040-8578/activities',
  'peer-reviews': {
    'last-modified-date': {
      value: 1607438831885,
    },
    group: [
      {
        'last-modified-date': {
          value: 1607438831885,
        },
        'external-ids': {
          'external-id': [
            {
              'external-id-type': 'peer-review',
              'external-id-value': 'issn:1520-5126',
              'external-id-normalized': null,
              'external-id-normalized-error': null,
              'external-id-url': null,
              'external-id-relationship': null,
            },
          ],
        },
        'peer-review-group': [
          {
            'last-modified-date': {
              value: 1594475686867,
            },
            'external-ids': {
              'external-id': [
                {
                  'external-id-type': 'other-id',
                  'external-id-value': 'ACS-20-38795123',
                  'external-id-normalized': {
                    value: 'acs-20-38795123',
                    transient: true,
                  },
                  'external-id-normalized-error': null,
                  'external-id-url': null,
                  'external-id-relationship': 'self',
                },
              ],
            },
            'peer-review-summary': [
              {
                'created-date': {
                  value: 1594475686867,
                },
                'last-modified-date': {
                  value: 1594475686867,
                },
                source: {
                  'source-orcid': null,
                  'source-client-id': {
                    uri: 'https://orcid.org/client/APP-UWGNMZLSYSXCJTL3',
                    path: 'APP-UWGNMZLSYSXCJTL3',
                    host: 'orcid.org',
                  },
                  'source-name': {
                    value: 'American Chemical Society',
                  },
                  'assertion-origin-orcid': null,
                  'assertion-origin-client-id': null,
                  'assertion-origin-name': null,
                },
                'reviewer-role': 'reviewer',
                'external-ids': {
                  'external-id': [
                    {
                      'external-id-type': 'other-id',
                      'external-id-value': 'ACS-20-38795123',
                      'external-id-normalized': {
                        value: 'acs-20-38795123',
                        transient: true,
                      },
                      'external-id-normalized-error': null,
                      'external-id-url': null,
                      'external-id-relationship': 'self',
                    },
                  ],
                },
                'review-url': null,
                'review-type': 'review',
                'completion-date': {
                  year: {
                    value: '2020',
                  },
                  month: null,
                  day: null,
                },
                'review-group-id': 'issn:1520-5126',
                'convening-organization': {
                  name: 'American Chemical Society',
                  address: {
                    city: 'Washington',
                    region: null,
                    country: 'US',
                  },
                  'disambiguated-organization': null,
                },
                visibility: 'public',
                'put-code': 2390161,
                path: '/0000-0002-1025-9484/peer-review/2390161',
                'display-index': '0',
              },
            ],
          },
          {
            'last-modified-date': {
              value: 1607438831885,
            },
            'external-ids': {
              'external-id': [
                {
                  'external-id-type': 'other-id',
                  'external-id-value': 'ACS-20-38904767',
                  'external-id-normalized': {
                    value: 'acs-20-38904767',
                    transient: true,
                  },
                  'external-id-normalized-error': null,
                  'external-id-url': null,
                  'external-id-relationship': 'self',
                },
              ],
            },
            'peer-review-summary': [
              {
                'created-date': {
                  value: 1607438831885,
                },
                'last-modified-date': {
                  value: 1607438831885,
                },
                source: {
                  'source-orcid': null,
                  'source-client-id': {
                    uri: 'https://orcid.org/client/APP-UWGNMZLSYSXCJTL3',
                    path: 'APP-UWGNMZLSYSXCJTL3',
                    host: 'orcid.org',
                  },
                  'source-name': {
                    value: 'American Chemical Society',
                  },
                  'assertion-origin-orcid': null,
                  'assertion-origin-client-id': null,
                  'assertion-origin-name': null,
                },
                'reviewer-role': 'reviewer',
                'external-ids': {
                  'external-id': [
                    {
                      'external-id-type': 'other-id',
                      'external-id-value': 'ACS-20-38904767',
                      'external-id-normalized': {
                        value: 'acs-20-38904767',
                        transient: true,
                      },
                      'external-id-normalized-error': null,
                      'external-id-url': null,
                      'external-id-relationship': 'self',
                    },
                  ],
                },
                'review-url': null,
                'review-type': 'review',
                'completion-date': {
                  year: {
                    value: '2020',
                  },
                  month: null,
                  day: null,
                },
                'review-group-id': 'issn:1520-5126',
                'convening-organization': {
                  name: 'American Chemical Society',
                  address: {
                    city: 'Washington',
                    region: null,
                    country: 'US',
                  },
                  'disambiguated-organization': null,
                },
                visibility: 'public',
                'put-code': 3024170,
                path: '/0000-0002-1025-9484/peer-review/3024170',
                'display-index': '0',
              },
            ],
          },
        ],
      },
      {
        'last-modified-date': {
          value: 1607265966228,
        },
        'external-ids': {
          'external-id': [
            {
              'external-id-type': 'peer-review',
              'external-id-value': 'issn:1520-6904',
              'external-id-normalized': null,
              'external-id-normalized-error': null,
              'external-id-url': null,
              'external-id-relationship': null,
            },
          ],
        },
        'peer-review-group': [
          {
            'last-modified-date': {
              value: 1607265966228,
            },
            'external-ids': {
              'external-id': [
                {
                  'external-id-type': 'other-id',
                  'external-id-value': 'ACS-20-38886330',
                  'external-id-normalized': {
                    value: 'acs-20-38886330',
                    transient: true,
                  },
                  'external-id-normalized-error': null,
                  'external-id-url': null,
                  'external-id-relationship': 'self',
                },
              ],
            },
            'peer-review-summary': [
              {
                'created-date': {
                  value: 1607265966228,
                },
                'last-modified-date': {
                  value: 1607265966228,
                },
                source: {
                  'source-orcid': null,
                  'source-client-id': {
                    uri: 'https://orcid.org/client/APP-UWGNMZLSYSXCJTL3',
                    path: 'APP-UWGNMZLSYSXCJTL3',
                    host: 'orcid.org',
                  },
                  'source-name': {
                    value: 'American Chemical Society',
                  },
                  'assertion-origin-orcid': null,
                  'assertion-origin-client-id': null,
                  'assertion-origin-name': null,
                },
                'reviewer-role': 'reviewer',
                'external-ids': {
                  'external-id': [
                    {
                      'external-id-type': 'other-id',
                      'external-id-value': 'ACS-20-38886330',
                      'external-id-normalized': {
                        value: 'acs-20-38886330',
                        transient: true,
                      },
                      'external-id-normalized-error': null,
                      'external-id-url': null,
                      'external-id-relationship': 'self',
                    },
                  ],
                },
                'review-url': null,
                'review-type': 'review',
                'completion-date': {
                  year: {
                    value: '2020',
                  },
                  month: null,
                  day: null,
                },
                'review-group-id': 'issn:1520-6904',
                'convening-organization': {
                  name: 'American Chemical Society',
                  address: {
                    city: 'Washington',
                    region: null,
                    country: 'US',
                  },
                  'disambiguated-organization': null,
                },
                visibility: 'public',
                'put-code': 3019203,
                path: '/0000-0002-1025-9484/peer-review/3019203',
                'display-index': '0',
              },
            ],
          },
          {
            'last-modified-date': {
              value: 1583950771706,
            },
            'external-ids': {
              'external-id': [
                {
                  'external-id-type': 'other-id',
                  'external-id-value': 'ACS-19-400031',
                  'external-id-normalized': {
                    value: 'acs-19-400031',
                    transient: true,
                  },
                  'external-id-normalized-error': null,
                  'external-id-url': null,
                  'external-id-relationship': 'self',
                },
              ],
            },
            'peer-review-summary': [
              {
                'created-date': {
                  value: 1583950771706,
                },
                'last-modified-date': {
                  value: 1583950771706,
                },
                source: {
                  'source-orcid': null,
                  'source-client-id': {
                    uri: 'https://orcid.org/client/APP-UWGNMZLSYSXCJTL3',
                    path: 'APP-UWGNMZLSYSXCJTL3',
                    host: 'orcid.org',
                  },
                  'source-name': {
                    value: 'American Chemical Society',
                  },
                  'assertion-origin-orcid': null,
                  'assertion-origin-client-id': null,
                  'assertion-origin-name': null,
                },
                'reviewer-role': 'reviewer',
                'external-ids': {
                  'external-id': [
                    {
                      'external-id-type': 'other-id',
                      'external-id-value': 'ACS-19-400031',
                      'external-id-normalized': {
                        value: 'acs-19-400031',
                        transient: true,
                      },
                      'external-id-normalized-error': null,
                      'external-id-url': null,
                      'external-id-relationship': 'self',
                    },
                  ],
                },
                'review-url': null,
                'review-type': 'review',
                'completion-date': {
                  year: {
                    value: '2019',
                  },
                  month: null,
                  day: null,
                },
                'review-group-id': 'issn:1520-6904',
                'convening-organization': {
                  name: 'American Chemical Society',
                  address: {
                    city: 'Washington',
                    region: null,
                    country: 'US',
                  },
                  'disambiguated-organization': null,
                },
                visibility: 'public',
                'put-code': 1680307,
                path: '/0000-0002-1025-9484/peer-review/1680307',
                'display-index': '0',
              },
            ],
          },
        ],
      },
      {
        'last-modified-date': {
          value: 1597121092036,
        },
        'external-ids': {
          'external-id': [
            {
              'external-id-type': 'peer-review',
              'external-id-value': 'issn:1439-7633',
              'external-id-normalized': null,
              'external-id-normalized-error': null,
              'external-id-url': null,
              'external-id-relationship': null,
            },
          ],
        },
        'peer-review-group': [
          {
            'last-modified-date': {
              value: 1597121092036,
            },
            'external-ids': {
              'external-id': [
                {
                  'external-id-type': 'source-work-id',
                  'external-id-value': '5d183277-1297-46bc-a7f8-8ede2198b198',
                  'external-id-normalized': {
                    value: '5d183277-1297-46bc-a7f8-8ede2198b198',
                    transient: true,
                  },
                  'external-id-normalized-error': null,
                  'external-id-url': {
                    value: '',
                  },
                  'external-id-relationship': 'self',
                },
              ],
            },
            'peer-review-summary': [
              {
                'created-date': {
                  value: 1597121092036,
                },
                'last-modified-date': {
                  value: 1597121092036,
                },
                source: {
                  'source-orcid': null,
                  'source-client-id': {
                    uri: 'https://orcid.org/client/APP-CK82NHIVJDRKJDIK',
                    path: 'APP-CK82NHIVJDRKJDIK',
                    host: 'orcid.org',
                  },
                  'source-name': {
                    value: 'Editorial Manager Journals at Wiley',
                  },
                  'assertion-origin-orcid': null,
                  'assertion-origin-client-id': null,
                  'assertion-origin-name': null,
                },
                'reviewer-role': 'reviewer',
                'external-ids': {
                  'external-id': [
                    {
                      'external-id-type': 'source-work-id',
                      'external-id-value':
                        '5d183277-1297-46bc-a7f8-8ede2198b198',
                      'external-id-normalized': {
                        value: '5d183277-1297-46bc-a7f8-8ede2198b198',
                        transient: true,
                      },
                      'external-id-normalized-error': null,
                      'external-id-url': {
                        value: '',
                      },
                      'external-id-relationship': 'self',
                    },
                  ],
                },
                'review-url': null,
                'review-type': 'review',
                'completion-date': {
                  year: {
                    value: '2020',
                  },
                  month: null,
                  day: null,
                },
                'review-group-id': 'issn:1439-7633',
                'convening-organization': {
                  name: 'Wiley-VCH',
                  address: {
                    city: 'Weinheim',
                    region: null,
                    country: 'DE',
                  },
                  'disambiguated-organization': null,
                },
                visibility: 'public',
                'put-code': 2516363,
                path: '/0000-0002-1025-9484/peer-review/2516363',
                'display-index': '0',
              },
            ],
          },
        ],
      },
      {
        'last-modified-date': {
          value: 1594129144846,
        },
        'external-ids': {
          'external-id': [
            {
              'external-id-type': 'peer-review',
              'external-id-value': 'issn:2374-7951',
              'external-id-normalized': null,
              'external-id-normalized-error': null,
              'external-id-url': null,
              'external-id-relationship': null,
            },
          ],
        },
        'peer-review-group': [
          {
            'last-modified-date': {
              value: 1594129144846,
            },
            'external-ids': {
              'external-id': [
                {
                  'external-id-type': 'other-id',
                  'external-id-value': 'ACS-20-22788113',
                  'external-id-normalized': {
                    value: 'acs-20-22788113',
                    transient: true,
                  },
                  'external-id-normalized-error': null,
                  'external-id-url': null,
                  'external-id-relationship': 'self',
                },
              ],
            },
            'peer-review-summary': [
              {
                'created-date': {
                  value: 1594129144846,
                },
                'last-modified-date': {
                  value: 1594129144846,
                },
                source: {
                  'source-orcid': null,
                  'source-client-id': {
                    uri: 'https://orcid.org/client/APP-UWGNMZLSYSXCJTL3',
                    path: 'APP-UWGNMZLSYSXCJTL3',
                    host: 'orcid.org',
                  },
                  'source-name': {
                    value: 'American Chemical Society',
                  },
                  'assertion-origin-orcid': null,
                  'assertion-origin-client-id': null,
                  'assertion-origin-name': null,
                },
                'reviewer-role': 'reviewer',
                'external-ids': {
                  'external-id': [
                    {
                      'external-id-type': 'other-id',
                      'external-id-value': 'ACS-20-22788113',
                      'external-id-normalized': {
                        value: 'acs-20-22788113',
                        transient: true,
                      },
                      'external-id-normalized-error': null,
                      'external-id-url': null,
                      'external-id-relationship': 'self',
                    },
                  ],
                },
                'review-url': null,
                'review-type': 'review',
                'completion-date': {
                  year: {
                    value: '2020',
                  },
                  month: null,
                  day: null,
                },
                'review-group-id': 'issn:2374-7951',
                'convening-organization': {
                  name: 'American Chemical Society',
                  address: {
                    city: 'Washington',
                    region: null,
                    country: 'US',
                  },
                  'disambiguated-organization': null,
                },
                visibility: 'public',
                'put-code': 2379707,
                path: '/0000-0002-1025-9484/peer-review/2379707',
                'display-index': '0',
              },
            ],
          },
          {
            'last-modified-date': {
              value: 1578586821755,
            },
            'external-ids': {
              'external-id': [
                {
                  'external-id-type': 'other-id',
                  'external-id-value': 'ACS-19-400032',
                  'external-id-normalized': {
                    value: 'acs-19-400032',
                    transient: true,
                  },
                  'external-id-normalized-error': null,
                  'external-id-url': null,
                  'external-id-relationship': 'self',
                },
              ],
            },
            'peer-review-summary': [
              {
                'created-date': {
                  value: 1578586821755,
                },
                'last-modified-date': {
                  value: 1578586821755,
                },
                source: {
                  'source-orcid': null,
                  'source-client-id': {
                    uri: 'https://orcid.org/client/APP-UWGNMZLSYSXCJTL3',
                    path: 'APP-UWGNMZLSYSXCJTL3',
                    host: 'orcid.org',
                  },
                  'source-name': {
                    value: 'American Chemical Society',
                  },
                  'assertion-origin-orcid': null,
                  'assertion-origin-client-id': null,
                  'assertion-origin-name': null,
                },
                'reviewer-role': 'reviewer',
                'external-ids': {
                  'external-id': [
                    {
                      'external-id-type': 'other-id',
                      'external-id-value': 'ACS-19-400032',
                      'external-id-normalized': {
                        value: 'acs-19-400032',
                        transient: true,
                      },
                      'external-id-normalized-error': null,
                      'external-id-url': null,
                      'external-id-relationship': 'self',
                    },
                  ],
                },
                'review-url': null,
                'review-type': 'review',
                'completion-date': {
                  year: {
                    value: '2019',
                  },
                  month: null,
                  day: null,
                },
                'review-group-id': 'issn:2374-7951',
                'convening-organization': {
                  name: 'American Chemical Society',
                  address: {
                    city: 'Washington',
                    region: null,
                    country: 'US',
                  },
                  'disambiguated-organization': null,
                },
                visibility: 'public',
                'put-code': 1330419,
                path: '/0000-0002-1025-9484/peer-review/1330419',
                'display-index': '0',
              },
            ],
          },
        ],
      },
      {
        'last-modified-date': {
          value: 1590898661682,
        },
        'external-ids': {
          'external-id': [
            {
              'external-id-type': 'peer-review',
              'external-id-value': 'issn:0175-7598',
              'external-id-normalized': null,
              'external-id-normalized-error': null,
              'external-id-url': null,
              'external-id-relationship': null,
            },
          ],
        },
        'peer-review-group': [
          {
            'last-modified-date': {
              value: 1590898661682,
            },
            'external-ids': {
              'external-id': [
                {
                  'external-id-type': 'source-work-id',
                  'external-id-value': 'e0d38650-4407-4edc-94a8-69d5ff5654dd',
                  'external-id-normalized': {
                    value: 'e0d38650-4407-4edc-94a8-69d5ff5654dd',
                    transient: true,
                  },
                  'external-id-normalized-error': null,
                  'external-id-url': {
                    value: '',
                  },
                  'external-id-relationship': 'self',
                },
              ],
            },
            'peer-review-summary': [
              {
                'created-date': {
                  value: 1590898661682,
                },
                'last-modified-date': {
                  value: 1590898661682,
                },
                source: {
                  'source-orcid': null,
                  'source-client-id': {
                    uri: 'https://orcid.org/client/APP-945VYTN20C7BZXYT',
                    path: 'APP-945VYTN20C7BZXYT',
                    host: 'orcid.org',
                  },
                  'source-name': {
                    value: 'Springer Nature',
                  },
                  'assertion-origin-orcid': null,
                  'assertion-origin-client-id': null,
                  'assertion-origin-name': null,
                },
                'reviewer-role': 'reviewer',
                'external-ids': {
                  'external-id': [
                    {
                      'external-id-type': 'source-work-id',
                      'external-id-value':
                        'e0d38650-4407-4edc-94a8-69d5ff5654dd',
                      'external-id-normalized': {
                        value: 'e0d38650-4407-4edc-94a8-69d5ff5654dd',
                        transient: true,
                      },
                      'external-id-normalized-error': null,
                      'external-id-url': {
                        value: '',
                      },
                      'external-id-relationship': 'self',
                    },
                  ],
                },
                'review-url': null,
                'review-type': 'review',
                'completion-date': {
                  year: {
                    value: '2020',
                  },
                  month: null,
                  day: null,
                },
                'review-group-id': 'issn:0175-7598',
                'convening-organization': {
                  name: 'Springer Nature',
                  address: {
                    city: 'New York',
                    region: null,
                    country: 'US',
                  },
                  'disambiguated-organization': {
                    'disambiguated-organization-identifier': 'grid.467660.5',
                    'disambiguation-source': 'GRID',
                  },
                },
                visibility: 'public',
                'put-code': 2203469,
                path: '/0000-0002-1025-9484/peer-review/2203469',
                'display-index': '0',
              },
            ],
          },
        ],
      },
      {
        'last-modified-date': {
          value: 1589171702556,
        },
        'external-ids': {
          'external-id': [
            {
              'external-id-type': 'peer-review',
              'external-id-value': 'issn:1521-3765',
              'external-id-normalized': null,
              'external-id-normalized-error': null,
              'external-id-url': null,
              'external-id-relationship': null,
            },
          ],
        },
        'peer-review-group': [
          {
            'last-modified-date': {
              value: 1589171702556,
            },
            'external-ids': {
              'external-id': [
                {
                  'external-id-type': 'source-work-id',
                  'external-id-value': '01df0575-496a-4d05-8e8c-7ca9a226ae23',
                  'external-id-normalized': {
                    value: '01df0575-496a-4d05-8e8c-7ca9a226ae23',
                    transient: true,
                  },
                  'external-id-normalized-error': null,
                  'external-id-url': {
                    value: '',
                  },
                  'external-id-relationship': 'self',
                },
              ],
            },
            'peer-review-summary': [
              {
                'created-date': {
                  value: 1589171702556,
                },
                'last-modified-date': {
                  value: 1589171702556,
                },
                source: {
                  'source-orcid': null,
                  'source-client-id': {
                    uri: 'https://orcid.org/client/APP-CK82NHIVJDRKJDIK',
                    path: 'APP-CK82NHIVJDRKJDIK',
                    host: 'orcid.org',
                  },
                  'source-name': {
                    value: 'Editorial Manager Journals at Wiley',
                  },
                  'assertion-origin-orcid': null,
                  'assertion-origin-client-id': null,
                  'assertion-origin-name': null,
                },
                'reviewer-role': 'reviewer',
                'external-ids': {
                  'external-id': [
                    {
                      'external-id-type': 'source-work-id',
                      'external-id-value':
                        '01df0575-496a-4d05-8e8c-7ca9a226ae23',
                      'external-id-normalized': {
                        value: '01df0575-496a-4d05-8e8c-7ca9a226ae23',
                        transient: true,
                      },
                      'external-id-normalized-error': null,
                      'external-id-url': {
                        value: '',
                      },
                      'external-id-relationship': 'self',
                    },
                  ],
                },
                'review-url': null,
                'review-type': 'review',
                'completion-date': {
                  year: {
                    value: '2020',
                  },
                  month: null,
                  day: null,
                },
                'review-group-id': 'issn:1521-3765',
                'convening-organization': {
                  name: 'Wiley-VCH',
                  address: {
                    city: 'Weinheim',
                    region: null,
                    country: 'DE',
                  },
                  'disambiguated-organization': null,
                },
                visibility: 'public',
                'put-code': 2166248,
                path: '/0000-0002-1025-9484/peer-review/2166248',
                'display-index': '0',
              },
            ],
          },
        ],
      },
      {
        'last-modified-date': {
          value: 1581002406621,
        },
        'external-ids': {
          'external-id': [
            {
              'external-id-type': 'peer-review',
              'external-id-value': 'issn:1520-4995',
              'external-id-normalized': null,
              'external-id-normalized-error': null,
              'external-id-url': null,
              'external-id-relationship': null,
            },
          ],
        },
        'peer-review-group': [
          {
            'last-modified-date': {
              value: 1581002406621,
            },
            'external-ids': {
              'external-id': [
                {
                  'external-id-type': 'other-id',
                  'external-id-value': 'ACS-19-400028',
                  'external-id-normalized': {
                    value: 'acs-19-400028',
                    transient: true,
                  },
                  'external-id-normalized-error': null,
                  'external-id-url': null,
                  'external-id-relationship': 'self',
                },
              ],
            },
            'peer-review-summary': [
              {
                'created-date': {
                  value: 1581002406621,
                },
                'last-modified-date': {
                  value: 1581002406621,
                },
                source: {
                  'source-orcid': null,
                  'source-client-id': {
                    uri: 'https://orcid.org/client/APP-UWGNMZLSYSXCJTL3',
                    path: 'APP-UWGNMZLSYSXCJTL3',
                    host: 'orcid.org',
                  },
                  'source-name': {
                    value: 'American Chemical Society',
                  },
                  'assertion-origin-orcid': null,
                  'assertion-origin-client-id': null,
                  'assertion-origin-name': null,
                },
                'reviewer-role': 'reviewer',
                'external-ids': {
                  'external-id': [
                    {
                      'external-id-type': 'other-id',
                      'external-id-value': 'ACS-19-400028',
                      'external-id-normalized': {
                        value: 'acs-19-400028',
                        transient: true,
                      },
                      'external-id-normalized-error': null,
                      'external-id-url': null,
                      'external-id-relationship': 'self',
                    },
                  ],
                },
                'review-url': null,
                'review-type': 'review',
                'completion-date': {
                  year: {
                    value: '2019',
                  },
                  month: null,
                  day: null,
                },
                'review-group-id': 'issn:1520-4995',
                'convening-organization': {
                  name: 'American Chemical Society',
                  address: {
                    city: 'Washington',
                    region: null,
                    country: 'US',
                  },
                  'disambiguated-organization': null,
                },
                visibility: 'public',
                'put-code': 1422657,
                path: '/0000-0002-1025-9484/peer-review/1422657',
                'display-index': '0',
              },
            ],
          },
        ],
      },
      {
        'last-modified-date': {
          value: 1579790959661,
        },
        'external-ids': {
          'external-id': [
            {
              'external-id-type': 'peer-review',
              'external-id-value': 'issn:1554-8937',
              'external-id-normalized': null,
              'external-id-normalized-error': null,
              'external-id-url': null,
              'external-id-relationship': null,
            },
          ],
        },
        'peer-review-group': [
          {
            'last-modified-date': {
              value: 1579790959661,
            },
            'external-ids': {
              'external-id': [
                {
                  'external-id-type': 'other-id',
                  'external-id-value': 'ACS-19-400030',
                  'external-id-normalized': {
                    value: 'acs-19-400030',
                    transient: true,
                  },
                  'external-id-normalized-error': null,
                  'external-id-url': null,
                  'external-id-relationship': 'self',
                },
              ],
            },
            'peer-review-summary': [
              {
                'created-date': {
                  value: 1579790959661,
                },
                'last-modified-date': {
                  value: 1579790959661,
                },
                source: {
                  'source-orcid': null,
                  'source-client-id': {
                    uri: 'https://orcid.org/client/APP-UWGNMZLSYSXCJTL3',
                    path: 'APP-UWGNMZLSYSXCJTL3',
                    host: 'orcid.org',
                  },
                  'source-name': {
                    value: 'American Chemical Society',
                  },
                  'assertion-origin-orcid': null,
                  'assertion-origin-client-id': null,
                  'assertion-origin-name': null,
                },
                'reviewer-role': 'reviewer',
                'external-ids': {
                  'external-id': [
                    {
                      'external-id-type': 'other-id',
                      'external-id-value': 'ACS-19-400030',
                      'external-id-normalized': {
                        value: 'acs-19-400030',
                        transient: true,
                      },
                      'external-id-normalized-error': null,
                      'external-id-url': null,
                      'external-id-relationship': 'self',
                    },
                  ],
                },
                'review-url': null,
                'review-type': 'review',
                'completion-date': {
                  year: {
                    value: '2019',
                  },
                  month: null,
                  day: null,
                },
                'review-group-id': 'issn:1554-8937',
                'convening-organization': {
                  name: 'American Chemical Society',
                  address: {
                    city: 'Washington',
                    region: null,
                    country: 'US',
                  },
                  'disambiguated-organization': null,
                },
                visibility: 'public',
                'put-code': 1358573,
                path: '/0000-0002-1025-9484/peer-review/1358573',
                'display-index': '0',
              },
            ],
          },
        ],
      },
      {
        'last-modified-date': {
          value: 1557549994361,
        },
        'external-ids': {
          'external-id': [
            {
              'external-id-type': 'peer-review',
              'external-id-value': 'issn:1521-3773',
              'external-id-normalized': null,
              'external-id-normalized-error': null,
              'external-id-url': null,
              'external-id-relationship': null,
            },
          ],
        },
        'peer-review-group': [
          {
            'last-modified-date': {
              value: 1557549994361,
            },
            'external-ids': {
              'external-id': [
                {
                  'external-id-type': 'source-work-id',
                  'external-id-value': '48715554-5c5f-4bb8-a20a-7c881f6d2fdf',
                  'external-id-normalized': {
                    value: '48715554-5c5f-4bb8-a20a-7c881f6d2fdf',
                    transient: true,
                  },
                  'external-id-normalized-error': null,
                  'external-id-url': {
                    value: '',
                  },
                  'external-id-relationship': 'self',
                },
              ],
            },
            'peer-review-summary': [
              {
                'created-date': {
                  value: 1557549994361,
                },
                'last-modified-date': {
                  value: 1557549994361,
                },
                source: {
                  'source-orcid': null,
                  'source-client-id': {
                    uri: 'https://orcid.org/client/APP-CK82NHIVJDRKJDIK',
                    path: 'APP-CK82NHIVJDRKJDIK',
                    host: 'orcid.org',
                  },
                  'source-name': {
                    value: 'Editorial Manager Journals at Wiley',
                  },
                  'assertion-origin-orcid': null,
                  'assertion-origin-client-id': null,
                  'assertion-origin-name': null,
                },
                'reviewer-role': 'reviewer',
                'external-ids': {
                  'external-id': [
                    {
                      'external-id-type': 'source-work-id',
                      'external-id-value':
                        '48715554-5c5f-4bb8-a20a-7c881f6d2fdf',
                      'external-id-normalized': {
                        value: '48715554-5c5f-4bb8-a20a-7c881f6d2fdf',
                        transient: true,
                      },
                      'external-id-normalized-error': null,
                      'external-id-url': {
                        value: '',
                      },
                      'external-id-relationship': 'self',
                    },
                  ],
                },
                'review-url': null,
                'review-type': 'review',
                'completion-date': {
                  year: {
                    value: '2019',
                  },
                  month: null,
                  day: null,
                },
                'review-group-id': 'issn:1521-3773',
                'convening-organization': {
                  name: 'Wiley-VCH',
                  address: {
                    city: 'Weinheim',
                    region: null,
                    country: 'DE',
                  },
                  'disambiguated-organization': null,
                },
                visibility: 'public',
                'put-code': 608707,
                path: '/0000-0002-1025-9484/peer-review/608707',
                'display-index': '0',
              },
            ],
          },
        ],
      },
    ],
    path: '/0000-0002-1025-9484/peer-reviews',
  },
};

const activitiesWithFundings = {
  path: '/0000-0003-0040-8578/activities',
  fundings: {
    'last-modified-date': {
      value: 1609537242482,
    },
    group: [
      {
        'last-modified-date': {
          value: 1609537058141,
        },
        'external-ids': {
          'external-id': [
            {
              'external-id-type': 'grant_number',
              'external-id-value': 'Grand1234',
              'external-id-normalized': null,
              'external-id-normalized-error': null,
              'external-id-url': null,
              'external-id-relationship': 'self',
            },
          ],
        },
        'funding-summary': [
          {
            'created-date': {
              value: 1609536991929,
            },
            'last-modified-date': {
              value: 1609537058141,
            },
            source: {
              'source-orcid': {
                uri: 'https://sandbox.orcid.org/0000-0003-0040-8578',
                path: '0000-0003-0040-8578',
                host: 'sandbox.orcid.org',
              },
              'source-client-id': null,
              'source-name': {
                value: 'Jirgn Mssnr',
              },
              'assertion-origin-orcid': null,
              'assertion-origin-client-id': null,
              'assertion-origin-name': null,
            },
            title: {
              title: {
                value: 'my awarded project',
              },
              'translated-title': null,
            },
            'external-ids': {
              'external-id': [
                {
                  'external-id-type': 'grant_number',
                  'external-id-value': 'Grand1234',
                  'external-id-normalized': null,
                  'external-id-normalized-error': null,
                  'external-id-url': null,
                  'external-id-relationship': 'self',
                },
              ],
            },
            url: {
              value: 'https://www.nobelprize.org/',
            },
            type: 'award',
            'start-date': {
              year: {
                value: '2019',
              },
              month: {
                value: '01',
              },
              day: null,
            },
            'end-date': {
              year: {
                value: '2022',
              },
              month: {
                value: '11',
              },
              day: null,
            },
            organization: {
              name: 'Noble Research Institute',
              address: {
                city: 'New York',
                region: null,
                country: 'US',
              },
              'disambiguated-organization': {
                'disambiguated-organization-identifier':
                  'http://dx.doi.org/10.13039/100012050',
                'disambiguation-source': 'FUNDREF',
              },
            },
            visibility: 'public',
            'put-code': 12151,
            path: '/0000-0003-0040-8578/funding/12151',
            'display-index': '1',
          },
        ],
      },
      {
        'last-modified-date': {
          value: 1609537242482,
        },
        'external-ids': {
          'external-id': [],
        },
        'funding-summary': [
          {
            'created-date': {
              value: 1609537242482,
            },
            'last-modified-date': {
              value: 1609537242482,
            },
            source: {
              'source-orcid': {
                uri: 'https://sandbox.orcid.org/0000-0003-0040-8578',
                path: '0000-0003-0040-8578',
                host: 'sandbox.orcid.org',
              },
              'source-client-id': null,
              'source-name': {
                value: 'Jirgn Mssnr',
              },
              'assertion-origin-orcid': null,
              'assertion-origin-client-id': null,
              'assertion-origin-name': null,
            },
            title: {
              title: {
                value: 'the other project with contract',
              },
              'translated-title': null,
            },
            'external-ids': null,
            url: null,
            type: 'contract',
            'start-date': {
              year: {
                value: '2020',
              },
              month: {
                value: '11',
              },
              day: null,
            },
            'end-date': {
              year: {
                value: '2024',
              },
              month: {
                value: '12',
              },
              day: null,
            },
            organization: {
              name: 'GEOMAR Helmholtz-Zentrum für Ozeanforschung Kiel',
              address: {
                city: 'Bremen',
                region: null,
                country: 'DE',
              },
              'disambiguated-organization': {
                'disambiguated-organization-identifier':
                  'http://dx.doi.org/10.13039/501100003153',
                'disambiguation-source': 'FUNDREF',
              },
            },
            visibility: 'public',
            'put-code': 12152,
            path: '/0000-0003-0040-8578/funding/12152',
            'display-index': '1',
          },
        ],
      },
    ],
    path: '/0000-0003-0040-8578/fundings',
  },
};

module.exports = {
  activitiesWithWorks,
  activitiesWithPeerReviews,
  activitiesWithFundings,
};
