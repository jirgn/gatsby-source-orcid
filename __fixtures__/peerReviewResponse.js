const peerReview = {
  'created-date': {
    value: 1594475686867,
  },
  'last-modified-date': {
    value: 1594475686867,
  },
  source: {
    'source-orcid': null,
    'source-client-id': {
      uri: 'https://orcid.org/client/APP-UWGNMZLSYSXCJTL3',
      path: 'APP-UWGNMZLSYSXCJTL3',
      host: 'orcid.org',
    },
    'source-name': {
      value: 'American Chemical Society',
    },
    'assertion-origin-orcid': null,
    'assertion-origin-client-id': null,
    'assertion-origin-name': null,
  },
  'reviewer-role': 'reviewer',
  'review-identifiers': {
    'external-id': [
      {
        'external-id-type': 'other-id',
        'external-id-value': 'ACS-20-38795123',
        'external-id-normalized': {
          value: 'acs-20-38795123',
          transient: true,
        },
        'external-id-normalized-error': null,
        'external-id-url': null,
        'external-id-relationship': 'self',
      },
    ],
  },
  'review-url': null,
  'review-type': 'review',
  'review-completion-date': {
    year: {
      value: '2020',
    },
    month: null,
    day: null,
  },
  'review-group-id': 'issn:1520-5126',
  'subject-external-identifier': null,
  'subject-container-name': {
    value: 'Journal of the American Chemical Society',
  },
  'subject-type': 'journal-article',
  'subject-name': null,
  'subject-url': null,
  'convening-organization': {
    name: 'American Chemical Society',
    address: {
      city: 'Washington',
      region: null,
      country: 'US',
    },
    'disambiguated-organization': null,
  },
  visibility: 'public',
  'put-code': 2390161,
  path: '/0000-0002-1025-9484/peer-review/2390161',
};

module.exports = {
  peerReview
};
