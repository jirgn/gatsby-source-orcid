const fundingResponse = {
  'created-date': {
    value: 1609537242482,
  },
  'last-modified-date': {
    value: 1609537242482,
  },
  source: {
    'source-orcid': {
      uri: 'https://sandbox.orcid.org/0000-0003-0040-8578',
      path: '0000-0003-0040-8578',
      host: 'sandbox.orcid.org',
    },
    'source-client-id': null,
    'source-name': {
      value: 'Jirgn Mssnr',
    },
    'assertion-origin-orcid': null,
    'assertion-origin-client-id': null,
    'assertion-origin-name': null,
  },
  'put-code': 12152,
  path: '/0000-0003-0040-8578/funding/12152',
  type: 'contract',
  'organization-defined-type': null,
  title: {
    title: {
      value: 'the other project with contract',
    },
    'translated-title': null,
  },
  'short-description':
    'Tortor. Class purus. Eros porttitor habitant, sociis vestibulum sollicitudin euismod, hymenaeos eros non vehicula. Erat libero hymenaeos faucibus cursus tortor tincidunt curabitur. Etiam purus cubilia est. Vestibulum natoque orci sociis id commodo. Nulla elementum est, euismod praesent dictum enim et sollicitudin aliquam. Nibh in massa magnis parturient velit. Eget tincidunt sociosqu. Vivamus aenean cum ornare mattis lacus primis laoreet lacinia. Lectus porttitor et. Habitant aptent, duis erat purus urna aliquam, eu elit pharetra. Blandit porta dui nibh nam mi a. Nisi, pede, nibh est rhoncus ad, interdum erat sapien torquent. Netus n',
  amount: {
    value: '100.0',
    'currency-code': 'EUR',
  },
  url: {
    value: 'https://theurl.com'
  },
  'start-date': {
    year: {
      value: '2020',
    },
    month: {
      value: '11',
    },
    day: null,
  },
  'end-date': {
    year: {
      value: '2024',
    },
    month: {
      value: '12',
    },
    day: null,
  },
  'external-ids': null,
  contributors: null,
  organization: {
    name: 'GEOMAR Helmholtz-Zentrum für Ozeanforschung Kiel',
    address: {
      city: 'Bremen',
      region: null,
      country: 'DE',
    },
    'disambiguated-organization': {
      'disambiguated-organization-identifier':
        'http://dx.doi.org/10.13039/501100003153',
      'disambiguation-source': 'FUNDREF',
    },
  },
  visibility: 'public',
};

module.exports = {
  fundingResponse,
};
