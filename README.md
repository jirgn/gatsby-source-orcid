# gatsby-source-orcid

## Description

This plugin sources data from [ORCID](https://orcid.org/), a
researcher network, that acts as central hub for
scientific publications.

The Data sourced is specific for a given `ORCID iD`, that is the
identifier of a researcher.

Using this Plugin, you get data of the person and his/her activites.
Supported activites are:
* works like publications (journal-articles, books, dissertation, ...), 
  itellectual properties (patents, licenses, trademarks, ...) 
  and others
* fundings (Awards, Contracts, ...)
* rudimetary info for peer reviews

*Hint*: The data needs to be marked as public by the reasearcher.

## How to install

```sh
npm install @jirgn/gatsby-source-orcid
```

## How to use

```javascript
// gatsby-config.js
module.exports = {
  plugins: [
    {
      resolve: "gatsby-source-orcid",
      options: {
        // isSandbox: true,
        // orcid: '0000-0003-0040-8578',
        isSandbox: false,
        orcid: '0000-0003-4216-8189'
      },
    },
  ],
};
```

## Available options
* *orcid*: (required) - identifier of the researcher on [orcid.org](https://orcid.org)
* *isSandbox*: (optional) defaults to `false` - flag indicates Usage of [orcid sandbox](https://sandbox.orcid.org).
  See also [Documentation on orcid.com](https://info.orcid.org/documentation/integration-guide/sandbox-testing-server/)


## How to query for data 

The plugin introduces the following types:
* *OrcidPerson*: main entrypoint accumulates all other types 
* *OrcidActivities*: aggregates OrcidFunding, OrcidPeerReview and OrcidWork
* *OrcidWork*: work like publications or intellectual properties
* *OrcidPeerReview*: peerreviews the researcher does for other publicatoins
* *OrcidFunding*: funding like awards or contracts

It is recommended to use OrcidPerson for your queries like

```graphql
  {
    orcidPerson {
      familyName
      givenNames
      identifiers {
        type
        value
      }
      activities {
        works {
          title {
            main
            subtitle
          }
          journalTitle
          type
          url
        }
        peerReviews {
          reviewCompletionDate
          subjectContainerName
          subjectName
          subjectUrl
        }
        fundings {
          shortDescription
          amount {
            value
            currencyCode
          }
          organization {
            name
            address
          }
        }
      }
    }
  }
```

## How to contribute

see our [Contribution Guidelines](./CONTRIBUTING.md)
