<!---
Please read this!

Before opening a new issue, make sure to search for keywords in the issues
filtered by the "regression" or "bug" label and verify the issue you're about to submit isn't a duplicate.

If you are submitting an issue with a tap, please include:

- account details
- target details
- entities selected  with meltano select (if you have selected any entities), as the bug may be related to a specific entity
- the full elt command you are running
- full output of the meltano elt command. Logs can get pretty long, so you can add the full log as a snippet in the Meltano project and add a link in the issue.
--->

### What is the current *bug* behavior?
(What is happening now)


### What is the expected *correct* behavior?
(What should be happening)


### Steps to reproduce
(How one can reproduce the issue)


### Relevant logs and/or screenshots
(Paste any relevant logs - please use code blocks (```) to format console output)


### Possible fixes
(If you can, link to the line of code that might be responsible for the problem or suggest a fix)

### Further regression test
(Ensure we automatically catch similar issues in the future)
- [ ] Write additional adequate test cases and submit test results
- [ ] Test results should be reviewed by a person from the team


/label ~bug
/label ~"To Do"

