const { chunkSplit } = require('../src/util');

describe('util', () => {
  describe('chunkSplit', () => {
    test('should split into chunks with rest', () => {
      const input = [1, 2, 3, 4, 5, 6, 7];
      const result = chunkSplit(input, 3);

      expect(result).toEqual([[1, 2, 3], [4, 5, 6], [7]]);
    });

    test('should wrap into nested array if short', () => {
      const input = [1, 2, 3];
      const result = chunkSplit(input, 3);

      expect(result).toEqual([[1, 2, 3]]);
    });
  });
});
