const {
  normalizePerson,
  normalizeWorksListing,
  normalizeWork,
  normalizePeerReviewsListing,
  normalizeFundingListing,
  normalizeFunding,
} = require('../src/normalize');

describe('normalize', () => {
  describe('normalizePerson', () => {
    test('should transform full person data', () => {
      const { personAll } = require('../__fixtures__/personResponse');
      const normalized = normalizePerson(personAll);

      expect(normalized).toEqual({
        path: '/0000-0003-0040-8578/person',
        orcid: '0000-0003-0040-8578',
        givenNames: 'Jirgn',
        familyName: 'Mssnr',
        activities: '/0000-0003-0040-8578/activities',
        biography:
          'This is the Biography.\nHere is some simple text that describes my live.',
        identifiers: [
          {
            type: 'Orcid',
            value: '0000-0003-0040-8578',
            url: 'https://orcid.org/0000-0003-0040-8578',
          },
          {
            type: 'ResearcherID',
            value: 'F-5108-2014',
            url: 'http://www.researcherid.com/rid/F-5108-2014',
          },
          {
            type: 'Scopus Author ID',
            value: '10641609900',
            url:
              'http://www.scopus.com/inward/authorDetails.url?authorID=10641609900&partnerID=MN8TOARS',
          },
        ],
      });
    });
  });

  describe('normalizeWorksListing', () => {
    test('should list works with paths and code', () => {
      const {
        activitiesWithWorks,
      } = require('../__fixtures__/activitiesResponse');
      const normalized = normalizeWorksListing(activitiesWithWorks);

      expect(normalized).toEqual([
        {
          path: '/0000-0003-0040-8578/work/1177076',
          putCode: 1177076,
        },
        {
          path: '/0000-0003-0040-8578/work/1177077',
          putCode: 1177077,
        },
        {
          path: '/0000-0003-0040-8578/work/1177314',
          putCode: 1177314,
        },
        {
          path: '/0000-0003-0040-8578/work/1177316',
          putCode: 1177316,
        },
      ]);
    });
  });

  describe('normalizeWork', () => {
    test('should transform full work data', () => {
      const { workAll } = require('../__fixtures__/workResponse');
      const normalized = normalizeWork(workAll);

      expect(normalized).toEqual({
        path: '/0000-0002-1025-9484/work/76054202',
        putCode: 76054202,
        orcid: '0000-0002-1025-9484',
        title: {
          main:
            'Four Phosphates at One Blow: Access to Pentaphosphorylated Magic Spot Nucleotides and Their Analysis by Capillary Electrophoresis',
          subtitle: null,
        },
        journalTitle: 'The Journal of Organic Chemistry',
        shortDescription: null,
        type: 'journal-article',
        publicationDate: { year: 2020, month: 11, day: 20 },
        url: 'https://doi.org/10.1021/acs.joc.0c00841',
        externalIds: [
          {
            type: 'doi',
            url: 'https://doi.org/10.1021/acs.joc.0c00841',
            value: '10.1021/acs.joc.0c00841',
          },
        ],

        contributors: [
          { creditName: 'Thomas M. Haas', role: 'author' },
          { creditName: 'Danye Qiu', role: 'author' },
          { creditName: 'Markus Häner', role: 'author' },
          { creditName: 'Larissa Angebauer', role: 'author' },
          { creditName: 'Alexander Ripp', role: 'author' },
          { creditName: 'Jyoti Singh', role: 'author' },
          { creditName: 'Hans-Georg Koch', role: 'author' },
          { creditName: 'Claudia Jessen-Trefzer', role: 'author' },
          { creditName: 'Henning J. Jessen', role: 'author' },
        ],
      });
    });
  });

  describe('normalizePeerReviewsListing', () => {
    test('should list all peerreviews with putCode, path', () => {
      const {
        activitiesWithPeerReviews,
      } = require('../__fixtures__/activitiesResponse');

      const normalized = normalizePeerReviewsListing(activitiesWithPeerReviews);
      expect(normalized).toEqual([
        {
          putCode: 2390161,
          path: '/0000-0002-1025-9484/peer-review/2390161',
        },
        {
          putCode: 3024170,
          path: '/0000-0002-1025-9484/peer-review/3024170',
        },
        {
          putCode: 3019203,
          path: '/0000-0002-1025-9484/peer-review/3019203',
        },
        {
          putCode: 1680307,
          path: '/0000-0002-1025-9484/peer-review/1680307',
        },
        {
          putCode: 2516363,
          path: '/0000-0002-1025-9484/peer-review/2516363',
        },
        {
          putCode: 2379707,
          path: '/0000-0002-1025-9484/peer-review/2379707',
        },
        {
          putCode: 1330419,
          path: '/0000-0002-1025-9484/peer-review/1330419',
        },
        {
          putCode: 2203469,
          path: '/0000-0002-1025-9484/peer-review/2203469',
        },
        {
          putCode: 2166248,
          path: '/0000-0002-1025-9484/peer-review/2166248',
        },
        {
          putCode: 1422657,
          path: '/0000-0002-1025-9484/peer-review/1422657',
        },
        {
          putCode: 1358573,
          path: '/0000-0002-1025-9484/peer-review/1358573',
        },
        {
          putCode: 608707,
          path: '/0000-0002-1025-9484/peer-review/608707',
        },
      ]);
    });
  });

  describe('normalizeFundingListing', () => {
    test('should list all fundings with putCode, path and title', () => {
      const {
        activitiesWithFundings,
      } = require('../__fixtures__/activitiesResponse');

      const normalized = normalizeFundingListing(activitiesWithFundings);
      expect(normalized).toEqual([
        {
          putCode: 12151,
          path: '/0000-0003-0040-8578/funding/12151',
        },
        {
          putCode: 12152,
          path: '/0000-0003-0040-8578/funding/12152',
        },
      ]);
    });
  });

  describe('normalizeFunding', () => {
    test('should transform full funding data', () => {
      const { fundingResponse } = require('../__fixtures__/fundingResponse');
      const normalized = normalizeFunding(fundingResponse);
      expect(normalized).toEqual({
        path: '/0000-0003-0040-8578/funding/12152',
        putCode: 12152,
        orcid: '0000-0003-0040-8578',
        type: 'contract',
        shortDescription:
          'Tortor. Class purus. Eros porttitor habitant, sociis vestibulum sollicitudin euismod, hymenaeos eros non vehicula. Erat libero hymenaeos faucibus cursus tortor tincidunt curabitur. Etiam purus cubilia est. Vestibulum natoque orci sociis id commodo. Nulla elementum est, euismod praesent dictum enim et sollicitudin aliquam. Nibh in massa magnis parturient velit. Eget tincidunt sociosqu. Vivamus aenean cum ornare mattis lacus primis laoreet lacinia. Lectus porttitor et. Habitant aptent, duis erat purus urna aliquam, eu elit pharetra. Blandit porta dui nibh nam mi a. Nisi, pede, nibh est rhoncus ad, interdum erat sapien torquent. Netus n',
        amount: { value: 10000, currencyCode: 'EUR' },
        url: 'https://theurl.com',
        startDate: { year: 2020, month: 11, day: null },
        endDate: { year: 2024, month: 12, day: null },
        organization: {
          name: 'GEOMAR Helmholtz-Zentrum für Ozeanforschung Kiel',
          address: { city: 'Bremen', region: null, country: 'DE' },
        },
      });
    });
  });
});
