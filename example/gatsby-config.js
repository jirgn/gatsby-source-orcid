module.exports = {
  plugins: [
    {
      // resolve: "gatsby-source-orcid",
      resolve: require.resolve('..'),
      options: {
        // isSandbox: true,
        // orcid: '0000-0003-0040-8578',
        isSandbox: false,
        orcid: '0000-0002-1025-9484'
      },
    },
  ],
};
