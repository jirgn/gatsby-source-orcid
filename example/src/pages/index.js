import { graphql } from 'gatsby';
import React from 'react';

export default ({ data: { orcidPerson } }) => {
  return (
    <div>
      <h1>Orcid Source Plugin</h1>
      <div>
        <p>
          Collecting data for{' '}
          <strong>
            {orcidPerson.familyName} {orcidPerson.givenNames}
          </strong>
          , identified by
        </p>
        <dl>
          {orcidPerson.identifiers.map(identifier => (
            <>
              <dt>{identifier.type}</dt>
              <dd>{identifier.value}</dd>
            </>
          ))}
        </dl>
      </div>
      <div>
        <h2>Activities</h2>
        The Researcher has
        <ul>
          <li>{orcidPerson.activities.works.length} Works</li>
          <li>{orcidPerson.activities.fundings.length} Fundings</li>
          <li>{orcidPerson.activities.peerReviews.length} Peer Reviews</li>
        </ul>
        <h3>Some Works</h3>
        {orcidPerson.activities.works.map((work, index) => {
          if (work.type === 'journal-article' && index < 5) {
            return (
              <article>
                <h4>{work.title.main}</h4>
                <h5>{work.title.subtitle}</h5>
                for journal '{work.journalTitle}'
                <br />
                <a href={work.url} target="_blank">Goto Article</a>
              </article>
            );
          }
        })}
      </div>
    </div>
  );
};

export const pageQuery = graphql`
  query IndexQuery {
    orcidPerson {
      familyName
      givenNames
      identifiers {
        type
        value
      }
      activities {
        works {
          title {
            main
            subtitle
          }
          journalTitle
          type
          url
        }
        peerReviews {
          path
        }
        fundings {
          path
        }
      }
    }
  }
`;
