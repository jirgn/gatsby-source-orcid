const axios = require('axios');
const { chunkSplit } = require('./util');

async function sleep(delay) {
  return new Promise(resolve => setTimeout(resolve, delay));
}

function readPublic(url, isSandbox) {
  return axios.get(url, {
    baseURL: isSandbox
      ? 'https://pub.sandbox.orcid.org/v3.0'
      : 'https://pub.orcid.org/v3.0',
    headers: {
      Accept: 'application/vnd.orcid+json',
    },
  });
}

async function activities(orcid, isSandbox = false) {
  console.info(`orcid - requesting activities for ${orcid}`);
  const result = await readPublic(`/${orcid}/activities`, isSandbox);
  return result.data;
}

/**
 * @param orcid {string} identifier of orcid member
 * @param putcodes {[int]} array of work putCodes
 * @param isSandbox {boolean} indicating if sanbox endpoint is used
 *
 * @return {} result
 */
async function worksBulk(orcid, putCodes, isSandbox = false) {
  console.info(
    `orcid - requesting bulk of works for ${orcid} total count ${putCodes.length}`
  );
  const putCodeChunks = chunkSplit(putCodes, 100);
  let result = [];

  for (const putCodeChunk of putCodeChunks) {
    let response = await readPublic(
      `/${orcid}/works/${putCodeChunk.join(',')}`,
      isSandbox
    );
    result.push(...response.data.bulk);
  }
  return result;
}

async function peerReviews(orcid, putCodes, isSandbox = false) {
  console.info(
    `orcid - requesting multiple peerReviews for ${orcid}. total count ${putCodes.length}`
  );
  const putCodeChunks = chunkSplit(putCodes, 10);
  let result = [];

  // we need to ensure to request only 20 per sec so the api limit is not reached
  // @see https://github.com/ORCID/ORCID-Source/tree/master/orcid-api-web#api-limits
  for (const chunk of putCodeChunks) {
    Promise.all(
      chunk.map(async putCode => {
        const peerReviewResponse = await readPublic(
          `/${orcid}/peer-review/${putCode}`,
          isSandbox
        );
        result.push(peerReviewResponse.data);
      })
    );
    await sleep(1000);
    console.info('peerReviews - sleeping 1s cause of api limits');
  }
  return result;
}

async function fundings(orcid, putCodes, isSandbox = false) {
  console.info(
    `orcid - requesting multiple fundings for ${orcid}. total count ${putCodes.length}`
  );
  const putCodeChunks = chunkSplit(putCodes, 10);
  let result = [];

  // we need to ensure to request only 20 per sec so the api limit is not reached
  // @see https://github.com/ORCID/ORCID-Source/tree/master/orcid-api-web#api-limits
  for (const chunk of putCodeChunks) {
    Promise.all(
      chunk.map(async putCode => {
        const fundingResponse = await readPublic(
          `/${orcid}/funding/${putCode}`,
          isSandbox
        );
        result.push(fundingResponse.data);
      })
    );
    await sleep(1000);
    console.info('fundings - sleeping 1s cause of api limits');
  }
  return result;
}

async function person(orcid, isSandbox = false) {
  console.info(`orcid - requesting person for ${orcid}`);
  const result = await readPublic(`/${orcid}/person`, isSandbox);
  return result.data;
}

module.exports = {
  activities,
  worksBulk,
  peerReviews,
  fundings,
  person,
};
