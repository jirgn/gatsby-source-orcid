function chunkSplit(array, size) {
  const itemCount = array.length;

  if (itemCount <= size) {
    return [array];
  }
  const fullCunks = Math.floor(itemCount / size);
  let chunks = [];

  for (let i = 0; i < fullCunks; i++) {
    let start = i * size;
    chunks.push(array.slice(start, start + size));
  }
  chunks.push(array.slice(fullCunks * size));

  return chunks;
}

module.exports = {
  chunkSplit,
};
