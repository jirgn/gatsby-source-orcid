function toDate(obj) {
  if (!obj) {
    return null;
  }
  return {
    year: obj.year ? extractValue(obj.year, parseInt) : null,
    month: obj.month ? extractValue(obj.month, parseInt) : null,
    day: obj.day ? extractValue(obj.day, parseInt) : null,
  };
}

function extractValue(obj, processFn = null, defaultValue = null) {
  if (!obj) {
    return defaultValue;
  }

  const value = obj.value;
  if (!value) {
    return defaultValue;
  }

  if (!processFn) {
    return value;
  }

  return processFn(value);
}

function givenOr(given, or) {
  return given ? given : or;
}

function tranformAmount(obj) {
  if (!obj) {
    return null;
  }

  return {
    value: parseInt(parseFloat(obj.value) * 100),
    currencyCode: obj['currency-code'],
  };
}

function transformExternalIdentifier(obj) {
  return {
    type: obj['external-id-type'],
    value: obj['external-id-value'],
    url: extractValue(obj['external-id-url']),
  };
}

function extractOrcidFromPath(path) {
  const pattern = /\/((?:\d{4}-){3}\d{4})(?:$|\/.*)/;
  const matches = path.match(pattern);
  if (matches && matches[1]) {
    return matches[1];
  }
  return null;
}

function normalizeActivities(activitiesResponse) {
  return {
    orcid: extractOrcidFromPath(activitiesResponse.path),
    path: activitiesResponse.path,
    works: normalizeWorksListing(activitiesResponse),
    peerReviews: normalizePeerReviewsListing(activitiesResponse),
    fundings: normalizeFundingListing(activitiesResponse),
  };
}

function normalizePeerReviewsListing(activitiesResponse) {
  const peerReviews = givenOr(activitiesResponse['peer-reviews'].group, []);
  let listing = [];

  for (const pr of peerReviews) {
    const peerReviewGroup = givenOr(pr['peer-review-group'], []);

    for (const summayWrapper of peerReviewGroup) {
      const summary = givenOr(summayWrapper['peer-review-summary'][0], false);

      if (summary) {
        listing.push({
          putCode: summary['put-code'],
          path: summary.path,
        });
      }
    }
  }
  return listing;
}

function normalizePeerReview(peerReviewResponse) {
  return {
    orcid: extractOrcidFromPath(peerReviewResponse.path),
    path: peerReviewResponse['path'],
    putCode: peerReviewResponse['put-code'],
    reviewerRole: peerReviewResponse['reviewer-role'],
    reviewUrl: peerReviewResponse['review-url'],
    reviewType: peerReviewResponse['review-type'],
    reviewCompletionDate: toDate(peerReviewResponse['review-completion-date']),
    subjectContainerName: extractValue(
      peerReviewResponse['subject-container-name']
    ),
    subjectType: peerReviewResponse['subject-type'],
    subjectName: peerReviewResponse['subject-name'],
    subjectUrl: peerReviewResponse['subject-url'],
  };
}

function normalizeFundingListing(activitiesResponse) {
  const fundings = givenOr(activitiesResponse['fundings'].group, []);
  let listing = [];

  for (const funding of fundings) {
    const summaryWrapper = givenOr(funding['funding-summary'], []);

    if (summaryWrapper.length !== 1) {
      const putCodes = summaryWrapper.map(item => item['put-code']);
      console.warn(
        `multiple funding-items in same summary: ${putCodes.join(',')}. Using ${
          putCodes[0]
        }`
      );
    }

    const summary = summaryWrapper[0];
    listing.push({
      putCode: summary['put-code'],
      path: summary.path,
    });
  }
  return listing;
}

function normalizeFunding(fundingResponse) {
  return {
    orcid: extractOrcidFromPath(fundingResponse.path),
    path: fundingResponse.path,
    putCode: fundingResponse['put-code'],
    type: fundingResponse.type,
    shortDescription: fundingResponse['short-description'],
    amount: tranformAmount(fundingResponse['amount']),
    url: extractValue(fundingResponse.url),
    startDate: toDate(fundingResponse['start-date']),
    endDate: toDate(fundingResponse['end-date']),
    organization: (function (obj) {
      if (!obj) {
        return null;
      }

      return {
        name: obj.name,
        address: obj.address,
      };
    })(fundingResponse.organization),
  };
}

function normalizeWorksListing(activitiesResponse) {
  const works = activitiesResponse.works ? activitiesResponse.works.group : [];
  let listing = [];

  for (const work of works) {
    const summaryWrapper = work['work-summary'];

    if (summaryWrapper.length !== 1) {
      const putCodes = summaryWrapper.map(item => item['put-code']);
      console.warn(
        `multiple workitems in same summary: ${putCodes.join(',')}. Using ${
          putCodes[0]
        }`
      );
    }

    const summary = summaryWrapper[0];
    listing.push({
      putCode: summary['put-code'],
      path: summary.path,
    });
  }
  return listing;
}

function normalizeWork(workResponse) {
  return {
    orcid: extractOrcidFromPath(workResponse.path),
    path: workResponse.path,
    putCode: workResponse['put-code'],
    title: {
      main: extractValue(workResponse.title.title),
      subtitle: extractValue(workResponse.title.subtitle),
    },
    journalTitle: extractValue(workResponse['journal-title']),
    shortDescription: workResponse['short-description'],
    type: workResponse.type,
    publicationDate: toDate(workResponse['publication-date']),
    url: extractValue(workResponse.url),
    externalIds: (function (workResponse) {
      return workResponse['external-ids']['external-id'].map(identifier =>
        transformExternalIdentifier(identifier)
      );
    })(workResponse),
    contributors: workResponse.contributors
      ? workResponse.contributors.contributor.map(contributor => {
          return {
            creditName: extractValue(contributor['credit-name']),
            role: contributor['contributor-attributes']
              ? contributor['contributor-attributes']['contributor-role']
              : null,
          };
        })
      : [],
  };
}

function normalizePerson(personResponse) {
  return {
    orcid: extractOrcidFromPath(personResponse.path),
    path: personResponse.path,
    givenNames: extractValue(personResponse.name['given-names']),
    familyName: extractValue(personResponse.name['family-name']),
    biography: personResponse.biography ? personResponse.biography.content : '',
    activities: `/${extractOrcidFromPath(personResponse.path)}/activities`,
    identifiers: (function (personResponse) {
      const externalIds = personResponse['external-identifiers'][
        'external-identifier'
      ].map(identifier => transformExternalIdentifier(identifier));
      return [
        {
          type: 'Orcid',
          value: personResponse.name.path,
          url: `https://orcid.org/${personResponse.name.path}`,
        },
        ...externalIds,
      ];
    })(personResponse),
  };
}

module.exports = {
  normalizePerson,
  normalizeActivities,
  normalizeWork,
  normalizeWorksListing,
  normalizePeerReviewsListing,
  normalizePeerReview,
  normalizeFundingListing,
  normalizeFunding,
};
